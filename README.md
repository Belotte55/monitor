# Monitor #

This is a monitor for Raspberry Pi.

![Capture d’écran 2015-11-04 à 15.42.06.png](https://bitbucket.org/repo/K9r5ra/images/3181185258-Capture%20d%E2%80%99%C3%A9cran%202015-11-04%20%C3%A0%2015.42.06.png)

# Config
You can custom the layout by modifying colors or city (for the weather).
Just edit infos.sh file.

# Warning
Please make sure to move files on a directory of the PATH.