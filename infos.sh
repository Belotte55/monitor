# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    infos.sh                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pi <fbellott@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/03 20:47:40 by pi                #+#    #+#              #
#    Updated: 2015/11/24 22:44:23 by pi               ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#!/bin/sh

city="Paris"

# Don't touch !
white='\033[0;1m'
black='\033[0;30m'
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
blue='\033[0;34m'
pink='\033[0;35m'
cyan='\033[0;36m'
grey='\033[0;37m'

# You can custom colors here:
color_var=$cyan
color_label=$green
color_key=$yellow

opt=''
if [ "$SHELL" = '/bin/bash' ]; then
	opt='-e'
fi

get_date ()
{
	am_pm=`date +%p | tr '[:lower:]' '[:upper:]'`
	date=`date +"${white}%B %_d, %Y - %I:%M ${am_pm}"`
	echo $opt $date
}

get_uptime ()
{
	# On récupère les variables qui vont afficher l'uptime;
	uptime=`cut -d. -f1 /proc/uptime`
	secs=$(($uptime%60))
	mins=$(($uptime/60%60))
	hours=$(($uptime/3600%24))
	days=$(($uptime/86400))

	# On place dans la variable $uptime l'uptime avec le format désiré;
	uptime=`/usr/bin/printf "${color_var}%d ${color_key}Jours ${grey}- ${color_var}%02d${grey}:${color_var}%02d${grey}" "$days" "$hours" "$mins"`
	echo $opt $uptime
}

get_mem ()
{
	total=`free | sed -n 2p | tr -s ' ' | cut -d ' ' -f2`
	total=$((total/1000))
	free=`free | sed -n 2p | tr -s ' ' | cut -d ' ' -f4`
	free=$((free/1000))
	mem="${color_var}${free}${color_key}MB ${grey}/ ${color_var}${total}${color_key}MB${grey}"
	echo $opt $mem
}

get_cpu ()
{
	cpu="${color_var}`top -n1 | grep Cpu | tr -s ' ' | cut -d ' ' -f2`${color_key}% ${grey}| ${color_var}`ps ax | wc -l` ${color_key}Processes${grey}"
	echo $opt $cpu
}

get_storage ()
{
	thousand="/usr/bin/printf %'d"
	total=`df -l | sed -n 2p | tr -s ' ' | cut -d ' ' -f2`
	total=$((total/1000))
	free=`df -l | sed -n 2p | tr -s ' ' | cut -d ' ' -f4`
	free=$((free/1000))
	storage="${color_var}`$thousand ${free}`${color_key}MB ${grey}/ ${color_var}`$thousand ${total}`${color_key}MB${grey}"
	storage=`echo $storage | tr ',' ' '`
	echo $opt $storage
}

get_wlan ()
{
	wlan="`/sbin/ifconfig | grep wlan0 -1 | grep inet | tr -s ' ' | \
		  cut -d ' ' -f3 | cut -d ':' -f2`"
	if [ "$wlan" = '' ]; then
		echo $opt "${white}Wlan -> ${red}Unplugged${grey}"
	else
		echo $opt "${white}Wlan -> ${color_var}${wlan}${grey}"
	fi
}

get_eth ()
{
	eth="${color_var}`/sbin/ifconfig | grep eth0 -1 | grep inet | tr -s ' ' | \
		 cut -d ' ' -f3 | cut -d ':' -f2`${grey}"
	if [ "$eth" = '' ]; then
		echo $opt "${white}Eth  -> ${red}Unplugged${grey}"
	else
		echo $opt "${white}Eth  -> ${color_var}${eth}${grey}"
	fi
}

get_ext ()
{
	ping=`ping 8.8.8.8 -c1 -W1 | grep ttl`
	if [ ! "$ping" = '' ]; then
		ext=`wget http://ipinfo.io/ip -qO -`
		echo $opt "${white}Ext  -> ${color_var}${ext}${grey}"
	else	
		echo $opt "${white}Ext  -> ${red}Unavailable${grey}"
	fi
}


get_temp ()
{
	temp=`vcgencmd measure_temp | cut -d '=' -f2 | cut -d "'" -f1`
	temp="${color_var}${temp}${color_key}ºC${grey}"
	echo $opt $temp
}

get_temp_in ()
{
	cd /sys/bus/w1/devices/28*
	if [ "$?" = "1" ]; then
		temp_in=''
		exit
	fi
	temp_in=`cat w1_slave  | grep t | cut -d'=' -f2`
	temp_in="$((temp_in/1000)).$(((temp_in/100)%10))"
}

get_weather ()
{
	ping=`ping 8.8.8.8 -c1 -W1 | grep ttl`
	if [ ! "$ping" = '' ]; then
		weather=`curl -s "http://rss.accuweather.com/rss/liveweather_rss.asp?metric=1&locCode=EUR|FR|FR001|$1" | grep 'Currently:'`
		weather_="${color_var}`echo $opt $weather | cut -d ':' -f2`${grey},${color_var}`echo $opt ${weather} | cut -d ':' -f3 | cut -d 'C' -f1`${color_key}ºC${grey}"
		get_temp_in
		if [ "$temp_in" = '' ]; then
			echo $opt "${color_label}Weather${grey}......:${weather_}"
		else
			echo $opt "${color_label}Weather${grey}......:${weather_} | ${color_key}Inside${grey}: ${color_var}${temp_in}${color_key}ºC"
		fi
	fi
}
get_weather
